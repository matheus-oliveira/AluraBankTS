import { Negociacao } from "../models/negociacao.js";
import { Negociacoes } from "../models/negociacoes.js";

export class NegociacaoController {
    private inputData: HTMLInputElement;
    private inptQuantidade: HTMLInputElement;
    private inputValor: HTMLInputElement;
    private negocicoes = new Negociacoes

    constructor() {
        this.inputValor = document.querySelector('#valor');
        this.inptQuantidade = document.querySelector('#quantidade');
        this.inputData = document.querySelector('#data');
    }

    adciona(): void {
        const negociacao = this.criaNegociacao();
        this.negocicoes.adiciona(negociacao);
        console.log(this.negocicoes.lista());
        this.limparFormulario();
    }

    criaNegociacao(): Negociacao {
        const exp = /-/g;
        const data = new Date(this.inputData.value.replace(exp, ','));
        const valor = parseFloat(this.inputValor.value);
        const quantidade = parseInt(this.inptQuantidade.value);
        return new Negociacao(data, quantidade, valor);
    }

    limparFormulario(): void {
        this.inputData.value = '';
        this.inptQuantidade.value = '';
        this.inputValor.value = '';
        this.inputData.focus();
    }

}
