export class Negociacao {
    public readonly data: Date;
    public readonly quantidade: number;
    public readonly valor: number;

    constructor(data: Date, quantidade: number, valor: number) {
        this.data = data;
        this.quantidade = quantidade;
        this.valor = valor;
    }

    get volume(): number {
        return this.valor * this.quantidade;
    }
}