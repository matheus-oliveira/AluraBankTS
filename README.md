Este projeto utiliza o nodeJS 10x

Inicie o container para executar o node e os comandos npm 

Para instalar as dependências
```
npm install 
```

Cria um servidor web para rodar o site
```
npm run server 
```

- ao colocar o "@” você especifica qual a versão será instalada
- ao colocar --save-dev o nodeJS entende que essa é uma dependência de desenvolvimento que quando o sistema for para produção ela não estará presente
```
npm install typescript@4.2.2 --save-dev
```

Compila TS em JS (a cada comando)
```
npm run compile 
```

Compila TS em JS (a cada modificação)
```
npm run watch 
```

Executa os comandos "server" e "watch" ao mesmo tempo 
```
npm run start 
```
# Simplificação de código  

Simplificação de construtor 
```
private _data: Date;
private _quantidade: number;
private _valor: number;

constructor(data: Date, quantidade: number, valor: number) {
    this._data = data;
    this._quantidade = quantidade;
    this._valor = valor;
}
```
para
 ```
constructor(private _data: Date,
    private _quantidade: number,
    private _valor: number) {
}
```

Simplificação de Array
```
private negociacoes: Array<Negociacao> = [];

lista(): ReadonlyArray<Negociacao> {
    return this.negociacoes;
}
```
para 
```
private negociacoes: Negociacao[] = [];

lista(): readonly Negociacao[] {
    return this.negociacoes;
}
```

Quando temos apenas métodos do tipo leitura (*getters*) podemos usar os atributos públicos (*public*) e torna-las apenas leitura com readonly
```
public readonly data: Date;
public readonly quantidade: number;
public readonly valor: number;
```
