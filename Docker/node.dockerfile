FROM node:10.24.1-buster

EXPOSE 3000 3001

RUN apt update && apt install -y\
    wget \
    nano \
    net-tools\
    curl

# RUN curl -fsSL https://deb.nodesource.com/setup_10.x | bash -
RUN apt install -y \
    # nodejs \
    npm 

RUN apt autoclean; apt autoremove; apt clean all

VOLUME ["/home"]
WORKDIR /home