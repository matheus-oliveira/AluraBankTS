import { Negociacao } from "../models/negociacao.js";
import { Negociacoes } from "../models/negociacoes.js";
export class NegociacaoController {
    constructor() {
        this.negocicoes = new Negociacoes;
        this.inputValor = document.querySelector('#valor');
        this.inptQuantidade = document.querySelector('#quantidade');
        this.inputData = document.querySelector('#data');
    }
    adciona() {
        const negociacao = this.criaNegociacao();
        this.negocicoes.adiciona(negociacao);
        console.log(this.negocicoes.lista());
        this.limparFormulario();
    }
    criaNegociacao() {
        const exp = /-/g;
        const data = new Date(this.inputData.value.replace(exp, ','));
        const valor = parseFloat(this.inputValor.value);
        const quantidade = parseInt(this.inptQuantidade.value);
        return new Negociacao(data, quantidade, valor);
    }
    limparFormulario() {
        this.inputData.value = '';
        this.inptQuantidade.value = '';
        this.inputValor.value = '';
        this.inputData.focus();
    }
}
